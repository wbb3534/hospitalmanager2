package com.privsq.hospitalmanager2.controller;

import com.privsq.hospitalmanager2.entity.HospitalCustomer;
import com.privsq.hospitalmanager2.model.HistoryInsuranceCorporationItem;
import com.privsq.hospitalmanager2.model.HistoryItem;
import com.privsq.hospitalmanager2.model.HistoryRequest;
import com.privsq.hospitalmanager2.model.HistoryResponse;
import com.privsq.hospitalmanager2.service.CustomerService;
import com.privsq.hospitalmanager2.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final CustomerService customerService;
    private final HistoryService historyService;

    @PostMapping("/new/customer-id/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer, request);

        return "OK";
    }

    @GetMapping("/all/date")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);
    }

    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByInsurance(searchDate);
    }

    @GetMapping("/detail/history-id/{historyId}")
    public HistoryResponse getHistory(@PathVariable long historyId) {
        return historyService.getHistory(historyId);
    }

    @PutMapping("/pay-complete/history-id/{historyId}")
    public String putHistoryByPayComplete(@PathVariable long historyId) {
        historyService.putHistoryByPayComplete(historyId);

        return "OK";
    }
}
