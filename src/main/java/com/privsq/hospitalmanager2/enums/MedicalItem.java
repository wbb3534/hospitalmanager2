package com.privsq.hospitalmanager2.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MedicalItem {
    PHYSICS("물리치료", 20000, 5000),
    CHUNA("추나치료", 70000, 70000),
    ACUPUNCTURE("약침치료", 20000, 3000)
    ;

    private final String name;
    private final double nonSalaryPrice;
    private final double salaryPrice;

}
