package com.privsq.hospitalmanager2.service;

import com.privsq.hospitalmanager2.entity.ClinicHistory;
import com.privsq.hospitalmanager2.entity.HospitalCustomer;
import com.privsq.hospitalmanager2.model.HistoryInsuranceCorporationItem;
import com.privsq.hospitalmanager2.model.HistoryItem;
import com.privsq.hospitalmanager2.model.HistoryRequest;
import com.privsq.hospitalmanager2.model.HistoryResponse;
import com.privsq.hospitalmanager2.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory();
        addData.setHospitalCustomer(hospitalCustomer);
        addData.setMedicalItem(historyRequest.getMedicalItem());
        addData.setPrice(historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice());
        addData.setIsSalary(historyRequest.getIsSalary());
        addData.setDateCure(LocalDate.now());
        addData.setTimeCure(LocalTime.now());
        addData.setIsCalculate(false);

        clinicHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setHistoryId(item.getId());
            addItem.setCustomerId(item.getHospitalCustomer().getId());
            addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setPrice(item.getPrice());
            addItem.setIsSalaryName(item.getIsSalary() ? "예" : "아니오");
            addItem.setDateCure(item.getDateCure());
            addItem.setTimeCure(item.getTimeCure());
            addItem.setIsCalculate(item.getIsCalculate() ? "예" : "아니오");

            result.add(addItem);
        }

        return result;
    }

    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(true, searchDate, true);

        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryInsuranceCorporationItem addItem = new HistoryInsuranceCorporationItem();
            addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setMedicalItemNonSalaryPrice(item.getMedicalItem().getNonSalaryPrice());
            addItem.setCustomerContributionPrice(item.getPrice());
            addItem.setBillingAmount(item.getMedicalItem().getNonSalaryPrice() - item.getPrice());
            addItem.setDateCure(item.getDateCure());

            result.add(addItem);
        }

        return result;
    }

    public HistoryResponse getHistory(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();

        HistoryResponse result = new HistoryResponse();
        result.setCustomerId(originData.getHospitalCustomer().getId());
        result.setCustomerName(originData.getHospitalCustomer().getCustomerName());
        result.setCustomerPhone(originData.getHospitalCustomer().getCustomerPhone());
        result.setRegistrationNumber(originData.getHospitalCustomer().getRegistrationNumber());
        result.setAddress(originData.getHospitalCustomer().getAddress());
        result.setMemo(originData.getHospitalCustomer().getMemo());
        result.setDateCreate(originData.getHospitalCustomer().getDateCreate());
        result.setHistoryId(originData.getId());
        result.setMedicalItemName(originData.getMedicalItem().getName());
        result.setOriginPrice(originData.getMedicalItem().getNonSalaryPrice());
        result.setCustomerPrice(originData.getPrice());
        result.setIsSalary(originData.getIsSalary() ? "Y" : "N");
        result.setDateCure(originData.getDateCure());
        result.setTimeCure(originData.getTimeCure());
        result.setIsCalculate(originData.getIsCalculate() ? "Y" : "N");

        return result;
    }

    public void putHistoryByPayComplete(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.setIsCalculate(true);

        clinicHistoryRepository.save(originData);
    }
}
