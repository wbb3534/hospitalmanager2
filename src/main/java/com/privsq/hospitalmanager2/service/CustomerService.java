package com.privsq.hospitalmanager2.service;

import com.privsq.hospitalmanager2.entity.HospitalCustomer;
import com.privsq.hospitalmanager2.model.CustomerRequest;
import com.privsq.hospitalmanager2.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();

        hospitalCustomerRepository.save(addData);
    }

    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }
}
