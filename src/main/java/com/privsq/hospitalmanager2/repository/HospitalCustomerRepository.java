package com.privsq.hospitalmanager2.repository;

import com.privsq.hospitalmanager2.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
