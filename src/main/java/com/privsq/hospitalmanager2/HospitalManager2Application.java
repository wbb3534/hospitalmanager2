package com.privsq.hospitalmanager2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManager2Application {

    public static void main(String[] args) {
        SpringApplication.run(HospitalManager2Application.class, args);
    }

}
