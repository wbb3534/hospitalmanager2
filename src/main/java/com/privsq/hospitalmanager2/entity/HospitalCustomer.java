package com.privsq.hospitalmanager2.entity;

import com.privsq.hospitalmanager2.interfaces.CommonModelBuilder;
import com.privsq.hospitalmanager2.model.CustomerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HospitalCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 13)
    private String customerPhone;

    @Column(nullable = false, length = 14)
    private String registrationNumber;

    @Column(nullable = false, length = 100)
    private String address;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;

    @Column(nullable = false)
    private LocalDate dateCreate;

    private HospitalCustomer(HospitalCustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo = builder.memo;
        this.dateCreate = builder.dateCreate;
    }

    public static class HospitalCustomerBuilder implements CommonModelBuilder<HospitalCustomer> {
        private final String customerName;
        private final String customerPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate dateCreate;

        public HospitalCustomerBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.registrationNumber = request.getRegistrationNumber();
            this.address = request.getAddress();
            this.memo = request.getMemo();
            this.dateCreate = LocalDate.now();
        }

        @Override
        public HospitalCustomer build() {
            return new HospitalCustomer(this);
        }
    }
}
