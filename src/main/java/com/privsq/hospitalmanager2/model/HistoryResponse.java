package com.privsq.hospitalmanager2.model;

import com.privsq.hospitalmanager2.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryResponse {
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String address;
    private String memo;
    private LocalDate dateCreate;
    private Long historyId;
    private String medicalItemName;
    private Double originPrice;
    private Double customerPrice;
    private String isSalary;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculate;
}
