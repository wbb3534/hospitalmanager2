package com.privsq.hospitalmanager2.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoryInsuranceCorporationItem {
    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String medicalItemName;
    private Double medicalItemNonSalaryPrice;
    private Double customerContributionPrice;
    private Double billingAmount;
    private LocalDate dateCure;
}
