package com.privsq.hospitalmanager2.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryItem {
    private Long historyId;
    private Long customerId;

    private String customerName;

    private String customerPhone;

    private String registrationNumber;

    private String medicalItemName;

    private Double price;

    private String isSalaryName;

    private LocalDate dateCure;

    private LocalTime timeCure;

    private String isCalculate;
}
