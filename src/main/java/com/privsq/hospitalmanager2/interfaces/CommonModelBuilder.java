package com.privsq.hospitalmanager2.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
